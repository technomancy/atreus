# Atreus case files

SVG and EPS files intended for laser cutting.

## Atreus Classic Cherry

* cherry-3mm.eps (in 3mm material)
* spacer.eps (either cut 2x in 3mm or cut in 6mm or 5mm)

## Atreus Classic Alps

* alps-switch-plate-3mm.eps
* alps-top-plate-3mm.eps
* spacer.eps (either cut 2x in 3mm or cut in 6mm or 5mm)
* bottom-plate-3mm.eps

## 44-key Atreus (Similar to keyboardio Atreus but not identical)

This is experimental and not well-tested.

* 44key-bottom.svg
* 44key-spacer.svg
* 44key-switch.svg
